import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/js/layout"
// import { graphql } from "gatsby"

export default ({ data }) => {
  console.log(data)
  return (
    <Layout>
      <h1>Category A page</h1>
      <p>
        This is Category A page
      </p>
      <h4>
        {data.allMarkdownRemark.totalCount} Posts
      </h4>
      <ul>
        {data.allMarkdownRemark.edges.map(({ node }, index) => (
          <li key={index}>
            <div>{node.frontmatter.title}</div>
            <div>{node.frontmatter.date}</div>
            <div>{node.frontmatter.category}</div>
            <div>[Contents]</div>
            <div>{node.excerpt}</div>
          </li>
          ))}
      </ul>
    </Layout>
  )
}

export const query = graphql`
query {
  allMarkdownRemark(filter: {frontmatter: {category: {eq: "category-a"}}}) {
    edges {
      node {
        id
        frontmatter {
          category
          date
          title
        }
        excerpt
      }
    }
    totalCount
  }
}
`