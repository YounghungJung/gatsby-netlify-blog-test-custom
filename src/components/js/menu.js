import React from "react"
import { Link, StaticQuery, graphql } from "gatsby"

export default () => {
  return (
    <StaticQuery
      query={
            graphql`
                query menusQuery {
                    allMenuJson {
                        edges {
                            node {
                                key
                                label
                                path
                            }
                        }
                    }
                }
            `
        }
      render={data => (
        <div className="menu-wrapper">
          <ul className="global-menus">
            {data.allMenuJson.edges.map(({ node }, index) => (
              <li 
                className="menu"
                key={index}
                label-key={node.key}
              >
                <Link to={node.path}>{node.label}</Link>
              </li>
                    ))}
                    
          </ul>
        </div>
        )}
    />
  )
}