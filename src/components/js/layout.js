import React from "react"
import { css } from "@emotion/core"
import { Link } from "gatsby"
import { rhythm } from "@utils/typography"
import Menu from "@components/js/menu"
import "@css/common.styl"

export default ({ children }) => (
  <div
    css={css`
      margin: 0 auto;
      max-width: 700px;
      padding: ${rhythm(2)};
      padding-top: ${rhythm(1.5)};
    `}
  >
    <Link to={`/`}>
      <h3
        css={css`
          margin-bottom: ${rhythm(2)};
          display: inline-block;
          font-style: normal;
        `}
      >
        Blog Test Page
      </h3>
    </Link>
    <Menu />
    {children}
  </div>
)